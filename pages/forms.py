from django import forms
from django.utils import timezone
from datetime import datetime

class PostForms(forms.Form):
    nama = forms.CharField(label = "Nama Kegiatan", max_length = 20)
    tempat = forms.CharField(label = "Tempat", max_length = 20)
    tanggal = forms.DateField(label = "Tanggal",
        widget = forms.DateInput(attrs={'type': 'date','class':'form-control','placeholder':'tanggal kegiatan'}))
    waktu   = forms.TimeField(label = "Waktu",
        widget = forms.TimeInput(attrs={'type': 'time','class':'form-control','placeholder':'tanggal kegiatan'}))
    deskripsi = forms.CharField(label = "Deskripsi",max_length = 40)