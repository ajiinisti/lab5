from django.urls import path
from pages.views import index,keterampilan,tentangsaya,pengalaman,jadwal,hapus
#url for app

urlpatterns = [
    # re_path(r'^$', index, name='index'),
    path('', index,name = 'index'),
    path('jadwal/', jadwal,name = 'jadwal'),
    path('keterampilan/', keterampilan, name='keterampilan'),
    path('tentangsaya/', tentangsaya , name='tentangsaya'),
    path('pengalaman/', pengalaman, name='pengalaman'),
    path('hapus/<getId>', hapus, name='hapus'),
]
