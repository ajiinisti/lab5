from django.shortcuts import render,redirect
from .forms import PostForms
from .models import PostModel

def index(request):
    return render(request, "index.html")

def pengalaman(request):
    return render(request, "pengalaman.html")

def keterampilan(request):
    return render(request, "keterampilan.html")

def tentangsaya(request):
    return render(request, "tentangsaya.html")

def jadwal(request):
    form = PostForms(request.POST or None)
    data = PostModel.objects.all()
    isian = {
            'generated_formulir': form,
            'generated_model' : data
        }
    if request.method == 'POST':
        model = PostModel(nama = form['nama'].value(),tempat = form['tempat'].value(),tanggal = form['tanggal'].value(), waktu = form['waktu'].value(), deskripsi= form['deskripsi'].value())
        model.save()
        return render(request, "jadwal.html", isian)
    elif data.count != 0:
        return render(request, "jadwal.html", isian)
    else:
        return render(request,"jadwal.html", {'generated_formulir': form})

def hapus(request,getId):
    data = PostModel.objects.get(pk = getId)
    data.delete()
    return redirect("jadwal")
    
    #data_dari_isian = request.GET[]
    #m = IsianSebelumnya(isian = data_dari_isian)
    #m.save()
    #dara_dari_isian_sebelum = IsianSebelumnya.objects.all() semua data sebbelumnya masuk sini
    #dictionary_a ={
    #   'masukan' : data_dari_isian,
    #   'dara_dari_isian_sebelum' : dara_dari_isian_sebelum
    #}#data_isian = request.GET['isian']
    #return render ()
